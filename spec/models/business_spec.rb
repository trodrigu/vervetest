require 'rails_helper'

RSpec.describe Business, :type => :model do
  it "is valid with a name, address, postal_code, country, latitude, longitude" do
    expect(build(:business)).to be_valid
  end
  it "is invalid without a name" do
    expect(build(:business, name: nil)).to be_invalid
  end
  describe "uploading of tsv" do
    before :each do
      path = 'files/offers_poi.tsv'
      type = 'text/tab-separated-values'
      @file = Rack::Test::UploadedFile.new(File.join(ActionController::TestCase.fixture_path, path), type)
    end
    it "can upload a tsv to the database" do
      expect {
        Business.upload @file
      }.to change(Business, :count).by(202)
    end
  end
  describe "ordering of locations" do
    before :each do
      @costco = create(:business, name: "Costco", address: "951 Palomar Airport Rd", latitude: 33.1208516, longitude:-117.3213859)
      @estadio_caliente = create(:business, address: "Blvd. Agua Caliente 12027", name: "Estadio Caliente", latitude: 32.5040964, longitude:-116.995013)
    end
    it "orders location by closeness to Verve" do
      expect(Business.close_to_home).to eq([@cheba_hut, @estadio_caliente])
    end
  end
end
