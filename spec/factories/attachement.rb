FactoryGirl.define do
  factory :attachment do
    supporting_documentation_file_name { 'offers_poi.tsv' }
    supporting_documentation_content_type { 'text/tsv' }
    supporting_documentation_file_size { 1024 }
  end
end
