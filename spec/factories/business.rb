FactoryGirl.define do
  factory :business do
    name "Mojopages"
    address "9171 Towne Center Dr"
    postal_code "92122"
    country "US"
    latitude 32.8721993
    longitude -117.2066254

    factory :invalid_business do
      name nil
    end
  end
end

