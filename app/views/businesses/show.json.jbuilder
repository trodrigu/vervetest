json.extract! @business, :id, :name, :address, :postal_code, :country, :latitude, :longitude, :created_at, :updated_at
