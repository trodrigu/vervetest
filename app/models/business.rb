class Business < ActiveRecord::Base
  validates_presence_of :name, :address, :postal_code, :country, :latitude, :longitude

  reverse_geocoded_by :latitude, :longitude
  #after_validation :reverse_geocode 

  def self.close_to_home
    Business.near(latitude: 33.1243208, longitude: -117.32582479999996)
  end

  def self.upload file
    if file.content_type == "text/tab-separated-values"
      tsv_text = File.read(file.tempfile)
      csv = CSV.parse(tsv_text, :col_sep => "\t")
      csv[1..-1].each do |v|
        name = v[0]
        #address_1 = nil
        address_1 = v[1]
        address_2 = v[2]
        postal_code = v[3]
        postal_code_suffix = v[4]
        phone_number = v[5]
        latitude = v[6]
        longitude = v[7]
        radius = v[8]
        # implement country save with lat/long and geocode
        country = 'US'
        business_params = {name: name, address: address_1, postal_code: postal_code, country: country, latitude: latitude, longitude: longitude}
        @business = Business.create!(business_params) 
      end
    else
      @business = nil
    end
  end
end
